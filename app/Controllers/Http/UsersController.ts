import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import {
  schema,
  rules, // 👈 import validator
} from '@ioc:Adonis/Core/Validator'


export default class UsersController {
  /**
   * @swagger
   * definitions:
   *   UserBody:
   *     properties:
   *       name:
   *         type: string
   *       email:
   *         type: string
   *       stack:
   *         type: string
   */
  /**
   * @swagger
   * /users:
   *   get:
   *     tags:
   *       - Users
   *     description: Returns all users
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: An array of users
   *         schema:
   *           $ref: '#/definitions/UserBody'
   */

  public async index() {
    const user = await User.query().preload('stacks')

    return user
  }

  /**
   * @swagger
   * /users:
   *   post:
   *     tags:
   *       - Users
   *     summary: Creates a new user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: Users object
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/UserBody'
   *     responses:
   *       200:
   *         description:  Successfully created
   */
  public async store({ request }: HttpContextContract) {
    const data = request.only(['name', 'email', 'stack'])
    const usersSchema = schema.create({
      name: schema.string(),
      email: schema.string({}, [rules.email()]),
      stack: schema.string(),
    })

    await request.validate({ schema: usersSchema })

    const user = new User()
    ;(user.name = data.name), (user.email = data.email)

    await user.related('stacks').create({
      name: data.stack,
    })
    return user
  }

  /**
   * @swagger
   * /users/{id}:
   *   get:
   *     tags:
   *       - Users
   *     description: Returns a single user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: User id
   *         in: path
   *         required: true
   *         type: integer
   *     responses:
   *       200:
   *         description: A single user
   *         schema:
   *           $ref: '#/definitions/UserBody'
   */
  public async show({ params }: HttpContextContract) {
    return await User.find(params.id)
  }

  /**
   * @swagger
   * /users/{id}:
   *   delete:
   *     tags:
   *       - Users
   *     description:  Deletes a single user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: User id
   *         in: path
   *         required: true
   *         type: integer
   *     responses:
   *       200:
   *         description: Successfully deleted
   */
  public async destroy({ params }: HttpContextContract) {
    const user = await User.find(params.id)
    await user?.related('stacks').detach()
    user?.delete()
  }

  /**
   * @swagger
   * /users/{id}:
   *   put:
   *     tags:
   *       - Users
   *     summary: Updates a single  user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         description: User id
   *         in: path
   *         required: true
   *         type: integer
   *       - name: user
   *         description: Users object
   *         in: body
   *         schema:
   *           type: array
   *           $ref: '#/definitions/UserBody'
   *     responses:
   *       200:
   *         description:  Successfully updated
   */
  public async update({ params, request }: HttpContextContract) {
    const data = request.only(['name', 'email', 'stack'])

    const user = await User.findOrFail(params.id)
    user.name = data.name
    user.email = data.email
    await user.save()
  }
}
