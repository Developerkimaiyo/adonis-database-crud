import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'

export default class UserSeeder extends BaseSeeder {
  public async run() {
    await User.createMany([
  
    { name: 'Maxwell Kimaiyo', email: 'maxwell@sendyit.com'},
    {  name: 'Gill Erick', email: 'erick@sendyit.com'  },
    {  name: 'Stacy Chebet', email: 'stacy@sendyit.com' },
    {  name: 'Dorcas Cherono"', email: 'dorcas@sendyit.com'},
  
    ])
  }
}
