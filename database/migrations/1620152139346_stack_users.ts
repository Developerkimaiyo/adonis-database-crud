import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class StackUser extends BaseSchema {
  protected tableName = 'stack_user'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
        table.integer('user_id').unsigned().references("id").inTable("users")
        table.integer('stack_id').unsigned().references("id").inTable("stacks")
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
