

import Route from '@ioc:Adonis/Core/Route'

import HealthCheck from '@ioc:Adonis/Core/HealthCheck'


Route.resource('users', 'UsersController').apiOnly()

// Route.get('swagger', async ({ view }) => {
//   return view.render('index')
// })


Route.get('health', async ({ response }) => {
  const report = await HealthCheck.getReport()

  return report.healthy ? response.ok(report) : response.badRequest(report)
})